package com.bolsadeideas.springboot.web.app.controllers;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.bolsadeideas.springboot.web.app.models.Usuario;

@Controller
@RequestMapping("/app")//Es una ruta que va a ser comun a todos los metodos de la clase
public class IndexController {

	//El nombre del método es el nombre de la vista

	//@RequestMapping(value="/index",method = RequestMethod.GET)//Por defecto es el GET

	//public String index(Model model) { //a traves del Model o ModelMap se le pueden pasar valores a la vista
	//Model es la que más se utiliza

	//public String index(ModelMap modelMap) { //Hereda de LinkedHashMap -> HashMap -> Map


	//public String index(Map<String, Object> map) {

	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;

	@GetMapping({"/index","/","/home"})//Cada operación CRUD tiene su etiqueta, da igual
	public String index(Model model){
		model.addAttribute("titulo", textoIndex);
		//modelMap.addAttribute("titulo","Hola Spring Framework, con ModelMap");
		//map.put("titulo", "Hola Spring Framework, con java.util.Map");
		//Tiene que devolver el nombre de una vista
		return "index";
	}

	//Con ModelAndView
//	@GetMapping({"/index","/","/home"})//Cada operación CRUD tiene su etiqueta, da igual
//	public ModelAndView index(ModelAndView mAndV){
//		mAndV.addObject("titulo","Hola Spring Framework con ModelAndView");
//		mAndV.setViewName("index");//Permite añadir el nombre de la vista
//		//Tiene que devolver el nombre de una vista
//		return mAndV;
//	}

	@RequestMapping("/perfil")
	public String perfil(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Andrés");
		usuario.setApellido("Guzman");
		usuario.setEmail("andrescorreo@correo.com");
		model.addAttribute("usuario",usuario);
		model.addAttribute("titulo",textoPerfil.concat(usuario.getNombre()));
		return "perfil";
	}

	@RequestMapping("/listar")
	public String listar(Model model) {
		/*List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario("Andrés","Guzmán","andres@correo.com"));
		usuarios.add(new Usuario("Jonh","Doe","john@correo.com"));
		usuarios.add(new Usuario("Jane","Doe","jane@correo.com"));*/
		//Otra forma
		@SuppressWarnings("unused")
		List<Usuario> usuarios = Arrays.asList(new Usuario("Andrés","Guzmán","andres@correo.com"),
				new Usuario("Jonh","Doe","john@correo.com"),new Usuario("Jane","Doe","jane@correo.com"),
				new Usuario("Tornado","Doe","tornado@correo.com"));

		model.addAttribute("titulo",textoListar);
		//model.addAttribute("usuarios",usuarios);
		return "listar";
	}

	//Esta manera sirve para pasar datos comunes a los controladores
	//Hacemos disponibles los datos que queramos a cualquier controlador
	//Nos evitamos tener que estar declarando siempre el addAttribute en cada metodo
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios(){
		List<Usuario> usuarios = Arrays.asList(new Usuario("Andrés","Guzmán","andres@correo.com"),
				new Usuario("Jonh","Doe","john@correo.com"),new Usuario("Jane","Doe","jane@correo.com"),
				new Usuario("Tornado","Doe","tornado@correo.com"));
		return usuarios;
	}
}
