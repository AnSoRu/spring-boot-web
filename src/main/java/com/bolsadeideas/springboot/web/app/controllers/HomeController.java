package com.bolsadeideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String home() {
		
		return "redirect:/app/index"; //Redirige hacia otro request
		//return "redirect:https://www.google.com";
		//Con el forward no hace una recarga de la pagina (no cambia la URL)
		//Se selecciona el dispatcher del controlador sin cambiar la ruta URL.
		//return "forward:/app/index";
	}
}
